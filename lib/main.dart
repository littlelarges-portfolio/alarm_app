import 'package:alarm/alarm.dart';
import 'package:alarm_app/logic/bloc/closest_alarm/closest_alarm_cubit.dart';
import 'package:alarm_app/logic/bloc/timer/timer_cubit.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:volume_controller/volume_controller.dart';

import 'logic/bloc/alarm/alarm_cubit.dart';
import 'logic/routes/app_router.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  HydratedBloc.storage = await HydratedStorage.build(
      storageDirectory: await getApplicationDocumentsDirectory());

  await Alarm.init();

  Alarm.stopAll();

  alarmBloc.init();

  timerBloc.init();

  runApp(const MyApp());
}

// AppData appData = AppData();
AppRouter appRouter = AppRouter();

VolumeController volumeController = VolumeController();

AlarmCubit alarmBloc = AlarmCubit();
TimerCubit timerBloc = TimerCubit();
ClosestAlarmCubit closestAlarmBloc = ClosestAlarmCubit();

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      theme: ThemeData(
        textTheme: GoogleFonts.rubikTextTheme(),
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        useMaterial3: true,
      ),
      routerConfig: appRouter.router,
    );
  }
}
