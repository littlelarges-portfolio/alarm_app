
import 'package:alarm_app/logic/alarm/alarm.dart';
import 'package:flutter/material.dart';
import 'package:wheel_picker/wheel_picker.dart';

import '../../main.dart';
import '../components/edit_page/ring_days_list.dart';
import '../components/edit_page/wheel.dart';

class EditPage extends StatefulWidget {
  EditPage({super.key, required this.alarmIndex});

  EditPage.add({super.key}) {
    isAddPage = true;
  }

  int alarmIndex = 0;
  bool isAddPage = false;

  @override
  State<EditPage> createState() => _EditPageState();
}

class _EditPageState extends State<EditPage> {
  final TextStyle wheelTextStyle = const TextStyle(fontSize: 30, height: 1.5);
  final TextStyle alarmInfoStyle = const TextStyle(fontSize: 20, height: 1.5);

  int dayIndex = -1;
  late final TextEditingController alarmNameController;
  final FocusNode alarmNameTextFieldFocusNode = FocusNode();

  late final WheelPickerController hourWheelController;
  late final WheelPickerController minuteWheelController;
  late final WheelPickerController periodWheelController;

  void _onFocusChanged() {
    if (alarmNameTextFieldFocusNode.hasFocus) {
      alarmNameController.selection = TextSelection(
          baseOffset: 0, extentOffset: alarmNameController.text.length);
    }
  }

  @override
  void initState() {
    super.initState();

    alarmNameTextFieldFocusNode.addListener(_onFocusChanged);

    if (widget.isAddPage) {
      alarmNameController = TextEditingController(text: '');

      DateTime now = DateTime.now();

      final int hour = now.hour > 12 ? now.hour - 12 : now.hour;

      hourWheelController =
          WheelPickerController(itemCount: 12, initialIndex: hour - 1);
      minuteWheelController =
          WheelPickerController(itemCount: 60, initialIndex: now.minute + 1);
      periodWheelController = WheelPickerController(
          itemCount: 2,
          initialIndex: now.hour < 12
              ? TimePeriodType.am.index
              : TimePeriodType.pm.index);
    } else {
      alarmNameController = TextEditingController(
          text: alarmBloc.state.appData.alarms[widget.alarmIndex].name);

      hourWheelController = WheelPickerController(
          itemCount: 12,
          initialIndex:
              alarmBloc.state.appData.alarms[widget.alarmIndex].time.hour - 1);
      minuteWheelController = WheelPickerController(
          itemCount: 60,
          initialIndex:
              alarmBloc.state.appData.alarms[widget.alarmIndex].time.minute);
      periodWheelController = WheelPickerController(
          itemCount: 2,
          initialIndex:
              alarmBloc.state.appData.alarms[widget.alarmIndex].period.index);
    }
  }

  @override
  void dispose() {
    alarmNameTextFieldFocusNode.removeListener(_onFocusChanged);
    alarmNameTextFieldFocusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    dayIndex = -1;

    return Scaffold(
        appBar: AppBar(
          title: const Text('Edit Alarm'),
        ),
        body: CustomScrollView(
          physics: const NeverScrollableScrollPhysics(),
          slivers: [
            SliverFillRemaining(
              hasScrollBody: false,
              child: Container(
                padding: const EdgeInsets.all(20),
                width: double.infinity,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Wheel(
                          controller: hourWheelController,
                          alarmIndex: widget.alarmIndex,
                          wheelTextStyle: wheelTextStyle,
                          type: WheelType.time,
                          isHour: true,
                        ),
                        const SizedBox(width: 10),
                        Wheel(
                          controller: minuteWheelController,
                          alarmIndex: widget.alarmIndex,
                          wheelTextStyle: wheelTextStyle,
                          type: WheelType.time,
                        ),
                        const SizedBox(width: 20),
                        Wheel(
                          controller: periodWheelController,
                          alarmIndex: widget.alarmIndex,
                          wheelTextStyle: wheelTextStyle,
                          type: WheelType.period,
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Alarm name',
                              style: alarmInfoStyle,
                            ),
                            SizedBox(
                                width: 140,
                                child: TextField(
                                  focusNode: alarmNameTextFieldFocusNode,
                                  controller: alarmNameController,
                                  maxLength: 10,
                                  textAlign: TextAlign.end,
                                  decoration: const InputDecoration(
                                      border: InputBorder.none),
                                  style: alarmInfoStyle,
                                )),
                          ],
                        ),
                        const SizedBox(height: 20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Rign days',
                              style: alarmInfoStyle,
                            ),
                            Column(
                              children: [
                                Text(
                                  'Select days',
                                  style: alarmInfoStyle,
                                ),
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  child: RingDaysList(
                                    alarmInfoStyle: alarmInfoStyle,
                                    alarmIndex: widget.alarmIndex,
                                    ringDays: alarmBloc.state.ringDays,
                                    // ringDays: RingDays(),
                                    onceFontSize: 18,
                                    ringDaysFontSize: 14,
                                    ringDaysPadding: 4,
                                    spacingWidth: 8,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ElevatedButton(
                            onPressed: () {
                              alarmBloc.cancel(context: context);
                            },
                            style: const ButtonStyle(
                                backgroundColor:
                                    MaterialStatePropertyAll(Colors.redAccent),
                                foregroundColor:
                                    MaterialStatePropertyAll(Colors.white),
                                shape: MaterialStatePropertyAll(
                                    RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10)))),
                                fixedSize:
                                    MaterialStatePropertyAll(Size(100, 60))),
                            child: const Icon(
                              Icons.cancel,
                              size: 50,
                            )),
                        ElevatedButton(
                            onPressed: () async {
                              if (widget.isAddPage) {
                                alarmBloc.save(
                                    alarmIndex: widget.alarmIndex,
                                    context: context,
                                    hourWheelController: hourWheelController,
                                    minuteWheelController:
                                        minuteWheelController,
                                    periodWheelController:
                                        periodWheelController,
                                    alarmNameController: alarmNameController,
                                    isAdding: true);
                              } else {
                                alarmBloc.save(
                                    alarmIndex: widget.alarmIndex,
                                    context: context,
                                    hourWheelController: hourWheelController,
                                    minuteWheelController:
                                        minuteWheelController,
                                    periodWheelController:
                                        periodWheelController,
                                    alarmNameController: alarmNameController);
                              }
                            },
                            style: const ButtonStyle(
                                backgroundColor:
                                    MaterialStatePropertyAll(Colors.lightGreen),
                                foregroundColor:
                                    MaterialStatePropertyAll(Colors.white),
                                shape: MaterialStatePropertyAll(
                                    RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10)))),
                                fixedSize:
                                    MaterialStatePropertyAll(Size(100, 60))),
                            child: const Icon(
                              Icons.done,
                              size: 50,
                            )),
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ));
  }
}
