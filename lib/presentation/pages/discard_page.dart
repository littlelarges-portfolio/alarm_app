
import 'package:alarm/model/alarm_settings.dart';
import 'package:alarm_app/main.dart';
import 'package:flutter/material.dart';

class DiscardPage extends StatefulWidget {
  const DiscardPage({super.key, required this.alarmSettings});

  final AlarmSettings alarmSettings;

  @override
  State<DiscardPage> createState() => _DiscardPageState();
}

class _DiscardPageState extends State<DiscardPage>
    with TickerProviderStateMixin {
  late final AnimationController buttonAnimationController;
  late final AnimationController infoAnimationController;
  late final AnimationController timeAnimationController;

  late final Animation buttonAnimation;
  late final Animation infoAnimation;
  late final Animation timeAnimation;

  late final double screenHeight;

  double _volumeListenerValue = 0;
  final double _getVolume = 0;
  double _setVolumeValue = 0;

  late final String hour, minute, period;

  @override
  void initState() {
    super.initState();

    buttonAnimationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 500));

    infoAnimationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 500));

    timeAnimationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 200));

    buttonAnimation = Tween(begin: 1.0, end: 0.0).animate(CurvedAnimation(
        parent: buttonAnimationController, curve: Curves.fastOutSlowIn));

    infoAnimation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: infoAnimationController, curve: Curves.fastOutSlowIn));

    timeAnimation = Tween(begin: -0.1, end: 0.1).animate(CurvedAnimation(
        parent: timeAnimationController, curve: Curves.fastOutSlowIn))
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          timeAnimationController.reverse();
        } else if (status == AnimationStatus.dismissed) {
          timeAnimationController.forward();
        }
      });

    buttonAnimationController.forward();
    infoAnimationController.forward();
    timeAnimationController.forward();

    volumeController.listener((volume) {
      setState(() => _volumeListenerValue = volume);
    });

    volumeController.getVolume().then((volume) => _setVolumeValue = volume);

    volumeController.setVolume(1);

    hour = (widget.alarmSettings.dateTime.hour > 12
            ? widget.alarmSettings.dateTime.hour - 12
            : widget.alarmSettings.dateTime.hour)
        .toString()
        .padLeft(2, '0');
    minute = widget.alarmSettings.dateTime.minute.toString().padLeft(2, '0');
    period = (widget.alarmSettings.dateTime.hour < 12 ? 'am' : 'pm');
  }

  @override
  void dispose() {
    volumeController.removeListener();

    buttonAnimationController.dispose();
    infoAnimationController.dispose();
    timeAnimationController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: WillPopScope(
          onWillPop: () async => false,
          child: SizedBox(
              width: double.infinity,
              height: double.infinity,
              child: Stack(
          alignment: Alignment.center,
          children: [
            AnimatedBuilder(
              animation: infoAnimation,
              builder: (context, child) {
                return Positioned(
                  top: 80,
                  child: Transform.scale(
                    scale: infoAnimation.value,
                    child: Text(widget.alarmSettings.notificationBody ?? '',
                        style: const TextStyle(
                            fontSize: 60, fontWeight: FontWeight.w100)),
                  ),
                );
              },
            ),
            AnimatedBuilder(
              animation: infoAnimation,
              builder: (context, child) {
                return AnimatedBuilder(
                  animation: timeAnimation,
                  builder: (context, child) {
                    return Positioned(
                      top: 200,
                      child: Transform.scale(
                        origin: Offset.zero,
                        scale: infoAnimation.value,
                        child: Transform.rotate(
                          angle: timeAnimation.value,
                          child: Text('$hour:$minute $period',
                              style: const TextStyle(
                                  fontSize: 80, fontWeight: FontWeight.w600)),
                        ),
                      ),
                    );
                  },
                );
              },
            ),
            AnimatedBuilder(
              animation: infoAnimation,
              builder: (context, child) {
                return Transform.scale(
                  scale: infoAnimation.value,
                  child: const Text('Tap to discard',
                      style: TextStyle(
                        fontSize: 40,
                      )),
                );
              },
            ),
            Positioned(
              bottom: 50,
              child: AnimatedBuilder(
                animation: buttonAnimation,
                builder: (context, child) {
                  return Transform.translate(
                    offset: Offset(0, buttonAnimation.value * 200.0),
                    child: ElevatedButton(
                        onPressed: () {
                          alarmBloc.discard(
                              context: context, alarmId: widget.alarmSettings.id);
                        },
                        style: const ButtonStyle(
                            backgroundColor: MaterialStatePropertyAll(Colors.red),
                            foregroundColor:
                                MaterialStatePropertyAll(Colors.white),
                            shape: MaterialStatePropertyAll(
                                RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(30)))),
                            fixedSize: MaterialStatePropertyAll(Size(260, 100))),
                        child: const Text(
                          'Discard',
                          style: TextStyle(
                            fontSize: 40,
                          ),
                        )),
                  );
                },
              ),
            )
          ],
              ),
            ),
        ));
  }
}

class DiscardPageExtra {
  const DiscardPageExtra({required this.alarmSettings});

  final AlarmSettings alarmSettings;
}
