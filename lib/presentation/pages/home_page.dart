
import 'package:alarm/alarm.dart';
import 'package:alarm_app/logic/bloc/closest_alarm/closest_alarm_cubit.dart';
import 'package:alarm_app/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import '../../logic/bloc/alarm/alarm_cubit.dart';
import '../components/home_page/alarm_card.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();

    Alarm.ringStream.stream.listen((alarmSettings) {
      final int alarmId = int.parse(alarmSettings.id
              .toString()
              .substring(0, alarmSettings.id.toString().length - 1)) -
          1;

      final int dayId = int.parse(
          alarmSettings.id.toString()[alarmSettings.id.toString().length - 1]);

      if (dayId == 0) {
        alarmBloc.disable(
          alarmId: alarmId,
        );
      } else {
        alarmBloc.setAlarm(alarmId: alarmId);
      }

      print('alarmSettings.id: ${alarmSettings.id}');

      context.push('/discard', extra: alarmSettings);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Container(
                padding: const EdgeInsets.all(10),
                height: 100,
                width: double.infinity,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  // color: Colors.red,
                ),
                child: BlocBuilder<ClosestAlarmCubit, ClosestAlarmState>(
                  builder: (context, state) {
                    if (state is ClosestAlarmIdleState) {
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text('The next alarm will ring in',
                              style: TextStyle(fontSize: 20)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                ' ${state.closestAlarmDateTime?.inDays.toString() ?? ''}',
                                style: const TextStyle(
                                    fontSize: 30,
                                    fontWeight: FontWeight.w700,
                                    height: 1.2),
                              ),
                              const Text(
                                ' day',
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.w600),
                              ),
                              Text(
                                ' ${state.closestAlarmDateTime?.inHours.remainder(24).toString() ?? ''}',
                                style: const TextStyle(
                                    fontSize: 30,
                                    fontWeight: FontWeight.w700,
                                    height: 1.2),
                              ),
                              const Text(
                                ' hour',
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.w600),
                              ),
                              Text(
                                ' ${state.closestAlarmDateTime?.inMinutes.remainder(60).toString() ?? ''}',
                                style: const TextStyle(
                                    fontSize: 30,
                                    fontWeight: FontWeight.w700,
                                    height: 1.2),
                              ),
                              const Text(
                                ' minute',
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.w600),
                              ),
                            ],
                          ),
                        ],
                      );
                    } else {
                      return const Center(
                        child: Text(
                          'No alarms set yet',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w600),
                        ),
                      );
                    }
                  },
                )),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   children: [
            //   ElevatedButton(
            //       onPressed: () {
            //         List<AlarmSettings> alarms = Alarm.getAlarms();

            //         for (AlarmSettings alarmSettings in alarms) {
            //           print(
            //               'dateTime: ${alarmSettings.dateTime} id: ${alarmSettings.id}');
            //         }
            //       },
            //       child: const Text('Get All Alarms')),
            //   ElevatedButton(
            //       onPressed: () {
            //         alarmBloc.init();
            //       },
            //       child: const Text('Sort')),
            // ]),
            const SizedBox(height: 20),
            Expanded(
              child: BlocBuilder<AlarmCubit, AlarmState>(
                builder: (context, state) {
                  return ListView.separated(
                    shrinkWrap: true,
                    physics: const BouncingScrollPhysics(),
                    itemCount: alarmBloc.state.appData.alarms.length + 1,
                    itemBuilder: ((context, index) {
                      for (var element in alarmBloc.state.appData.alarms) {
                        print(element.name);
                      }

                      return index == alarmBloc.state.appData.alarms.length
                          ? const SizedBox(height: 50)
                          : AlarmCard(index: index);
                    }),
                    separatorBuilder: (BuildContext context, int index) {
                      return const SizedBox(height: 20);
                    },
                  );
                },
              ),
            ),
          ],
        ),
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          alarmBloc.add(context: context);
        },
        tooltip: 'Add Alarm',
        child: const Icon(Icons.add),
      ),
    );
  }
}
