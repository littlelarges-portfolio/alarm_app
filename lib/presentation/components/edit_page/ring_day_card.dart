import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../logic/alarm/ring_days.dart';
import '../../../logic/bloc/alarm/alarm_cubit.dart';
import '../../../main.dart';

class RingDayCard extends StatelessWidget {
  const RingDayCard({
    super.key,
    required this.name,
    required this.ringDayCardIndex,
    required this.alarmInfoStyle,
    required this.dialogSetState,
    required this.ringDays,
  });

  final String name;
  final int ringDayCardIndex;
  final TextStyle alarmInfoStyle;
  final Function dialogSetState;
  final RingDays ringDays;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        alarmBloc.toggleRingDay(ringDayCardIndex: ringDayCardIndex);
      },
      child: Container(
          padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 10),
          width: double.infinity,
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              // color: Colors.red,
              border: Border.all(width: .1)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(name, style: alarmInfoStyle),
              IgnorePointer(
                child: BlocBuilder<AlarmCubit, AlarmState>(
                  builder: (context, state) {
                    return Checkbox(
                        value: alarmBloc
                            .state.ringDays.days[ringDayCardIndex].status,
                        onChanged: (value) {});
                  },
                ),
              )
            ],
          )),
    );
  }
}
