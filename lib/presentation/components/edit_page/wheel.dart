import 'package:flutter/material.dart';
import 'package:wheel_picker/wheel_picker.dart';

import '../../../logic/alarm/alarm.dart';

class Wheel extends StatelessWidget {
  const Wheel(
      {super.key,
      required this.controller,
      required this.alarmIndex,
      required this.wheelTextStyle,
      required this.type,
      this.isHour = false});

  final WheelPickerController controller;
  final int alarmIndex;
  final TextStyle wheelTextStyle;
  final WheelType type;
  final bool isHour;

  @override
  Widget build(BuildContext context) {
    return WheelPicker(
      controller: controller,
      looping: type == WheelType.time ? true : false,
      builder: (context, index) => Text(
        type == WheelType.time
            ? (isHour ? index + 1 : index).toString().padLeft(2, '0')
            : (TimePeriodType.values[index].name),
        style: wheelTextStyle,
      ),
      selectedIndexColor: Colors.black,
      style: WheelPickerStyle(
        height: 200,
        squeeze: 1.25,
        diameterRatio: .8,
        itemExtent: wheelTextStyle.fontSize! * wheelTextStyle.height!,
        surroundingOpacity: .25,
        magnification: 1.2,
      ),
    );
  }
}

enum WheelType { time, period }
