import 'package:flutter/material.dart';

import '../../../logic/alarm/ring_days.dart';
import '../../../main.dart';
import 'select_ring_days_dialog.dart';

class RingDaysList extends StatefulWidget {
  const RingDaysList(
      {super.key,
      required this.alarmInfoStyle,
      required this.alarmIndex,
      required this.ringDays,
      required this.onceFontSize,
      required this.ringDaysFontSize,
      required this.ringDaysPadding,
      required this.spacingWidth});

  final TextStyle alarmInfoStyle;
  final int alarmIndex;
  final RingDays ringDays;
  final double onceFontSize;
  final double ringDaysFontSize;
  final double ringDaysPadding;
  final double spacingWidth;

  @override
  State<RingDaysList> createState() => _RingDaysListState();
}

class _RingDaysListState extends State<RingDaysList> {
  int dayIndex = -1;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    dayIndex = -1;

    return GestureDetector(
      onTap: () {
        alarmBloc.editRingDays(
            editRingDaysExtra: EditRingDaysExtra(
                alarmIndex: widget.alarmIndex,
                alarmInfoStyle: widget.alarmInfoStyle,
                ringDays: widget.ringDays),
            context: context);
      },
      child: Container(
        // padding: const EdgeInsets.all(8),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          // color: Colors.green,
        ),
        child: Row(
          children: widget.ringDays.allRingDaysFalse()
              ? [
                  Container(
                    padding: const EdgeInsets.all(4),
                    child: Text('Once',
                        style: widget.alarmInfoStyle.copyWith(
                            fontSize: widget.onceFontSize,
                            color: Colors.black)),
                  ),
                ]
              : List.generate(
                  widget.ringDays.allRingDaysFalse()
                      ? 0
                      : widget.ringDays.days.length +
                          widget.ringDays.days.length -
                          1,
                  ((index) {
                    bool isEven = index.isEven;

                    if (isEven) {
                      ++dayIndex;
                    }

                    return isEven
                        ? Container(
                            padding: EdgeInsets.all(widget.ringDaysPadding),
                            decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                            ),
                            child: Text(
                              widget.ringDays.days[dayIndex].name,
                              style: widget.alarmInfoStyle.copyWith(
                                  fontSize: widget.ringDaysFontSize,
                                  color: widget.ringDays.days[dayIndex].status
                                      ? Colors.black
                                      : Colors.black.withOpacity(.1)),
                            ),
                          )
                        : SizedBox(width: widget.spacingWidth);
                  }),
                ),
        ),
      ),
    );
  }
}
