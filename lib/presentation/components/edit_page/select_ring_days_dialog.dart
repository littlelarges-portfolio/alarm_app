// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../logic/alarm/ring_days.dart';
import '../../../logic/bloc/alarm/alarm_cubit.dart';
import '../../../main.dart';
import 'ring_day_card.dart';

class SelectRingDaysDialog extends StatefulWidget {
  const SelectRingDaysDialog({
    super.key,
    required this.context,
    required this.alarmIndex,
    required this.alarmInfoStyle,
    required this.ringDays,
  });

  final BuildContext context;
  final int alarmIndex;
  final TextStyle alarmInfoStyle;
  final RingDays ringDays;

  @override
  State<SelectRingDaysDialog> createState() => _SelectRingDaysDialogState();
}

class _SelectRingDaysDialogState extends State<SelectRingDaysDialog> {
  @override
  void initState() {
    super.initState();

    print('dialog ring days: ${widget.ringDays.hashCode}');
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text('Ring Days'),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  BlocBuilder<AlarmCubit, AlarmState>(
                    builder: (context, state) {
                      return ToggleButtons(
                        selectedColor: Colors.blue,
                        isSelected: alarmBloc.state.weekdays,
                        onPressed: (toggleIndex) {
                          alarmBloc.switchWeek(
                              weekType: WeekType.days,
                              toggleIndex: toggleIndex);
                        },
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 10),
                            child: Text('Weekdays',
                                style: widget.alarmInfoStyle
                                    .copyWith(fontSize: 16)),
                          ),
                        ],
                      );
                    },
                  ),
                  BlocBuilder<AlarmCubit, AlarmState>(
                    builder: (context, state) {
                      return ToggleButtons(
                        selectedColor: Colors.blue,
                        isSelected: alarmBloc.state.weekends,
                        onPressed: (toggleIndex) {
                          alarmBloc.switchWeek(
                              weekType: WeekType.ends,
                              toggleIndex: toggleIndex);
                        },
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 10),
                            child: Text('Weekends',
                                style: widget.alarmInfoStyle
                                    .copyWith(fontSize: 16)),
                          ),
                        ],
                      );
                    },
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RingDayCard(
                    name: 'Monday',
                    ringDayCardIndex: 0,
                    alarmInfoStyle: widget.alarmInfoStyle,
                    dialogSetState: setState,
                    ringDays: widget.ringDays,
                  ),
                  const SizedBox(height: 10),
                  RingDayCard(
                    name: 'Tuesday',
                    ringDayCardIndex: 1,
                    alarmInfoStyle: widget.alarmInfoStyle,
                    dialogSetState: setState,
                    ringDays: widget.ringDays,
                  ),
                  const SizedBox(height: 10),
                  RingDayCard(
                    name: 'Wednesday',
                    ringDayCardIndex: 2,
                    alarmInfoStyle: widget.alarmInfoStyle,
                    dialogSetState: setState,
                    ringDays: widget.ringDays,
                  ),
                  const SizedBox(height: 10),
                  RingDayCard(
                    name: 'Thursday',
                    ringDayCardIndex: 3,
                    alarmInfoStyle: widget.alarmInfoStyle,
                    dialogSetState: setState,
                    ringDays: widget.ringDays,
                  ),
                  const SizedBox(height: 10),
                  RingDayCard(
                    name: 'Friday',
                    ringDayCardIndex: 4,
                    alarmInfoStyle: widget.alarmInfoStyle,
                    dialogSetState: setState,
                    ringDays: widget.ringDays,
                  ),
                  const SizedBox(height: 10),
                  RingDayCard(
                    name: 'Saturday',
                    ringDayCardIndex: 5,
                    alarmInfoStyle: widget.alarmInfoStyle,
                    dialogSetState: setState,
                    ringDays: widget.ringDays,
                  ),
                  const SizedBox(height: 10),
                  RingDayCard(
                    name: 'Sunday',
                    ringDayCardIndex: 6,
                    alarmInfoStyle: widget.alarmInfoStyle,
                    dialogSetState: setState,
                    ringDays: widget.ringDays,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      style: const ButtonStyle(
                          fixedSize: MaterialStatePropertyAll(Size(240, 40))),
                      onPressed: () {
                        alarmBloc.saveRingDays(context: context);
                      },
                      child: const Text('Done')),
                ],
              )
            ]),
      ),
    );
  }
}

class EditRingDaysExtra {
  EditRingDaysExtra(
      {required this.alarmIndex,
      required this.ringDays,
      this.alarmInfoStyle = const TextStyle()});

  final int alarmIndex;
  final RingDays ringDays;
  final TextStyle alarmInfoStyle;

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'alarmIndex': alarmIndex,
      'ringDays': ringDays.toMap(),
    };
  }

  factory EditRingDaysExtra.fromMap(Map<String, dynamic> map) {
    return EditRingDaysExtra(
      alarmIndex: map['alarmIndex'] as int,
      ringDays: RingDays.fromMap(map['ringDays'] as Map<String,dynamic>),
    );
  }

  String toJson() => json.encode(toMap());

  factory EditRingDaysExtra.fromJson(String source) => EditRingDaysExtra.fromMap(json.decode(source) as Map<String, dynamic>);
}
