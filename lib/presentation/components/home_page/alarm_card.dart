import 'package:alarm_app/presentation/components/edit_page/ring_days_list.dart';
import 'package:flutter/material.dart';

import '../../../main.dart';

class AlarmCard extends StatelessWidget {
  const AlarmCard({required this.index, super.key});

  final int index;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        alarmBloc.edit(alarmIndex: index, context: context);
      },
      child: Stack(
        children: [
          Container(
              height: 105,
              decoration: BoxDecoration(
                border: Border.all(width: .2),
                borderRadius: const BorderRadius.all(Radius.circular(10)),
                color: alarmBloc.state.appData.alarms[index].enabled
                    ? Colors.white
                    : Colors.white,
              ),
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            alarmBloc
                                    .state.appData.alarms[index].name.isNotEmpty
                                ? alarmBloc.state.appData.alarms[index].name
                                : 'Alarm',
                          ),
                          IgnorePointer(
                            child: RingDaysList(
                              alarmInfoStyle: const TextStyle(fontSize: 10),
                              alarmIndex: index,
                              ringDays: alarmBloc
                                  .state.appData.alarms[index].ringDays,
                              onceFontSize: 12,
                              ringDaysFontSize: 10,
                              ringDaysPadding: 2,
                              spacingWidth: 4,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                              '${(alarmBloc.state.appData.alarms[index].time.hour).remainder(12).abs().toString().padLeft(2, '0')}:${alarmBloc.state.appData.alarms[index].time.minute.remainder(60).abs().toString().padLeft(2, '0')} ${alarmBloc.state.appData.alarms[index].period.name}',
                              style: const TextStyle(
                                  fontSize: 24, fontWeight: FontWeight.w600)),
                          const SizedBox(width: 6),
                          SizedBox(
                            height: 30,
                            child: FittedBox(
                              fit: BoxFit.fill,
                              child: Switch(
                                value: alarmBloc
                                    .state.appData.alarms[index].enabled,
                                onChanged: (value) {
                                  alarmBloc.toggle(
                                      alarmIndex: index, toggle: value);
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ]),
              )),
          Positioned(
            right: 0,
            bottom: 0,
            child: PopupMenuButton(
                // surfaceTintColor: Colors.red,
                itemBuilder: (context) {
              return [
                PopupMenuItem(
                    onTap: () {
                      alarmBloc.delete(alarmIndex: index);
                    },
                    child: const Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [Text('Delete'), Icon(Icons.delete)],
                    ))
              ];
            }),
          )
        ],
      ),
    );
  }
}
