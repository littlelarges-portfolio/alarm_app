part of 'closest_alarm_cubit.dart';

class ClosestAlarmState {
  Duration? closestAlarmDateTime;

  void setClosestAlarm() {
    closestAlarmDateTime = getClosestAlarm();
  }

  Duration getClosestAlarm() {
    print(alarmBloc);
    if (alarmBloc.state.appData.activatedAlarms.isNotEmpty) {
      DateTime now = DateTime.now();

      DateTime roundNow = DateTime(
        now.year,
        now.month,
        now.day,
        now.hour,
        now.minute,
      );

      List<DateTime> alarmTimes = [];

      for (var alarm in alarmBloc.state.appData.activatedAlarms) {
        if (alarm.ringDays.days.any((day) => day.status)) {
          alarmTimes.addAll(alarm.ringDays.days
              .where((day) => day.status == true)
              .map((e) => e.time)
              .toList());
        } else {
          alarmTimes = alarmBloc.state.appData.activatedAlarms
              .map(
                (alarm) => alarm.time,
              )
              .toList();
        }
      }

      Duration closestDifference = (alarmTimes[0].difference(roundNow)).abs();

      for (int i = 1; i < alarmTimes.length; i++) {
        Duration difference = (alarmTimes[i].difference(roundNow)).abs();

        if (difference < closestDifference) {
          closestDifference = difference;
        }
      }

      return closestDifference;
    }

    return const Duration();
  }
}

final class ClosestAlarmInitialState extends ClosestAlarmState {}

final class ClosestAlarmIdleState extends ClosestAlarmState {}

final class ClosestAlarmEmptyState extends ClosestAlarmState {}
