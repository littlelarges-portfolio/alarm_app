
import 'package:bloc/bloc.dart';

import '../../../main.dart';

part 'closest_alarm_state.dart';

class ClosestAlarmCubit extends Cubit<ClosestAlarmState> {
  ClosestAlarmCubit() : super(ClosestAlarmInitialState());

  updateClosestAlarm() {
    if (alarmBloc.state.appData.activatedAlarms.isNotEmpty) {
      emit(ClosestAlarmIdleState()..setClosestAlarm());
    } else {
      emit(ClosestAlarmEmptyState());
    }
  }
}
