import 'dart:async';
import 'dart:convert';

import 'package:alarm/alarm.dart';
import 'package:alarm_app/logic/alarm/day.dart';
import 'package:alarm_app/main.dart';
import 'package:alarm_app/presentation/components/edit_page/select_ring_days_dialog.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:wheel_picker/wheel_picker.dart';

import '../../alarm/alarm.dart';
import '../../alarm/ring_days.dart';
import '../../app_data/app_data.dart';

part 'alarm_event.dart';
part 'alarm_state.dart';

class AlarmCubit extends HydratedCubit<AlarmState> {
  AlarmCubit() : super(IdleState(appData: AppData(alarms: [], activatedAlarms: []),));

  void edit({required int alarmIndex, required BuildContext context}) =>
      emit(IdleState(appData: state.appData,)
        ..edit(alarmIndex: alarmIndex, context: context));

  Future<void> save(
          {required int alarmIndex,
          required BuildContext context,
          required WheelPickerController hourWheelController,
          required WheelPickerController minuteWheelController,
          required WheelPickerController periodWheelController,
          required TextEditingController alarmNameController,
          bool isAdding = false}) async =>
      emit(IdleState.copy(appData: state.appData, ringDays: state.ringDays)
        ..save(
            alarmIndex: alarmIndex,
            context: context,
            hourWheelController: hourWheelController,
            minuteWheelController: minuteWheelController,
            periodWheelController: periodWheelController,
            alarmNameController: alarmNameController,
            isAdding: isAdding));

  void toggle({required int alarmIndex, required bool toggle}) =>
      emit(IdleState(appData: state.appData)
        ..toggle(alarmIndex: alarmIndex, toggle: toggle));

  void cancel({required BuildContext context}) =>
      emit(IdleState(appData: state.appData)..cancel(context: context));

  void discard({required BuildContext context, required int alarmId}) =>
      emit(IdleState(appData: state.appData)
        ..discard(context: context, alarmId: alarmId));

  void editRingDays(
          {required EditRingDaysExtra editRingDaysExtra,
          required BuildContext context}) =>
      emit(IdleState.copy(appData: state.appData, ringDays: state.ringDays)
        ..editRingDays(editRingDaysExtra: editRingDaysExtra, context: context));

  void saveRingDays({required BuildContext context}) =>
      emit(IdleState.copy(appData: state.appData, ringDays: state.ringDays)
        ..saveRingDays(context: context));

  void switchWeek({required WeekType weekType, required int toggleIndex}) =>
      emit(IdleState.copy(
          appData: state.appData,
          ringDays: state.ringDays,
          weekdays: state.weekdays,
          weekends: state.weekends)
        ..switchWeek(weekType: weekType, toggleIndex: toggleIndex));

  void toggleRingDay({required int ringDayCardIndex}) => emit(IdleState.copy(
      appData: state.appData,
      ringDays: state.ringDays,
      weekdays: state.weekdays,
      weekends: state.weekends)
    ..toggleRingDay(ringDayCardIndex: ringDayCardIndex));

  void add({required BuildContext context}) =>
      emit(IdleState(appData: state.appData)..add(context: context));

  void delete({required int alarmIndex}) =>
      emit(IdleState(appData: state.appData)..delete(alarmIndex: alarmIndex));

  void init() => emit(IdleState(appData: state.appData)..init());

  void disable({required int alarmId}) => emit(IdleState(appData: state.appData)..toggle(alarmIndex: alarmId, toggle: false));

  void setAlarm({required int alarmId}) => emit(IdleState(appData: state.appData)..setAlarms(state.appData.alarms[alarmId]));

  void sort() => emit(IdleState(appData: state.appData)..sort());
  
  @override
  AlarmState? fromJson(Map<String, dynamic> json) {
    return AlarmState.fromMap(json);
  }
  
  @override
  Map<String, dynamic>? toJson(AlarmState state) {
    return state.toMap();
  }
}
