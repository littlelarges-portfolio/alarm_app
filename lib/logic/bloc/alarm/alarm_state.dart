part of 'alarm_cubit.dart';

class AlarmState {
  AlarmState({required this.appData});

  AlarmState.copy({required this.appData, required this.ringDays});

  final AppData appData;

  late RingDays ringDays;

  late List<bool> weekdays;
  late List<bool> weekends;

  void checkWeekdaysAndWeekends() {
    weekdays = [ringDays.weekdayRingDaysTrue()];
    weekends = [ringDays.weekendRingDaysTrue()];
  }

  int compareStatus(bool a, bool b) {
    if (a == b) return 0;

    if (a) {
      return -1;
    } else {
      return 1;
    }
  }

  int compareTime(DateTime a, DateTime b) {
    if (a == b) return 0;

    final int aSum = a.hour + a.minute;
    final int bSum = b.hour + b.minute;

    if (aSum < bSum) {
      return -1;
    } else {
      return 1;
    }
  }

  int comparePeriod(TimePeriodType a, TimePeriodType b) {
    if (a == b) return 0;

    if (a == TimePeriodType.am) {
      return -1;
    } else {
      return 1;
    }
  }

  void sortByStatus(List<AlarmInfo> list) {
    list.sort((a, b) => compareStatus(a.enabled, b.enabled));
  }

  void sortByTime(List<AlarmInfo> list) {
    list.sort((a, b) => compareTime(a.time, b.time));
  }

  void sortByPeriod(List<AlarmInfo> list) {
    list.sort((a, b) => comparePeriod(a.period, b.period));
  }

  void sort() {
    sortByTime(appData.alarms);
    sortByPeriod(appData.alarms);
    sortByStatus(appData.alarms);
  }

  Future<void> save({
    required int alarmIndex,
    required BuildContext context,
    required WheelPickerController hourWheelController,
    required WheelPickerController minuteWheelController,
    required WheelPickerController periodWheelController,
    required TextEditingController alarmNameController,
    required bool isAdding,
  }) async {
    context.pop();

    print('hour selected: ${hourWheelController.selected}');
    print('hour selected: ${minuteWheelController.selected}');

    int hours = (hourWheelController.selected + 1).remainder(12);
    int minutes = minuteWheelController.selected.remainder(60);
    int period = periodWheelController.selected.remainder(2);

    if (isAdding) {
      AlarmInfo alarm = AlarmInfo(
        name: alarmNameController.text,
        enabled: true,
        time: DateTime(
            0,
            0,
            0,
            hours +
                (TimePeriodType.values[period] == TimePeriodType.pm ? 12 : 0),
            minutes),
        period: TimePeriodType.values[period],
        ringDays: ringDays,
      );

      if (notContainAlarm(alarm)) {
        appData.alarms.add(alarm);
        addToActivatedAlarms(alarm);

        alarmIndex = appData.alarms.length - 1;
      } else {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text('Such an alarm clock already exists')));
      }
    } else {
      final int activatedAlarmIndex =
          appData.activatedAlarms.indexOf(appData.alarms[alarmIndex]);

      AlarmInfo alarm = AlarmInfo(
          name: alarmNameController.text,
          enabled: true,
          time: DateTime(
              0,
              0,
              0,
              hours +
                  (TimePeriodType.values[period] == TimePeriodType.pm ? 12 : 0),
              minutes),
          period: TimePeriodType.values[period],
          ringDays: ringDays);

      notContainAlarm(alarm);

      if (notContainAlarm(alarm)) {
        addToActivatedAlarms(alarm);

        appData.alarms[alarmIndex] = alarm;
        appData.activatedAlarms[activatedAlarmIndex] = alarm;
      } else {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('Such an alarm clock already exists'),
          backgroundColor: Colors.red,
        ));
      }
    }

    AlarmInfo alarm = appData.alarms[alarmIndex];

    DateTime now = DateTime.now();

    DateTime selectedAlarmTime = DateTime(now.year, now.month, now.day,
        period == 0 ? (hours + 1) : (hours + 1) + 12, minutes);

    int compareResult = selectedAlarmTime.compareTo(now);

    DateTime finalAlarmTime = DateTime(
        now.year,
        now.month,
        compareResult == 1
            ? selectedAlarmTime.day
            : selectedAlarmTime.add(const Duration(days: 1)).day,
        period == 0 ? (hours + 1) : (hours + 1) + 12,
        minutes);

    print('alarmIndex is: $alarmIndex');

    setAllAlarms();
  }

  bool notContainAlarm(AlarmInfo alarm) {
    return !appData.alarms.any((element) {
      return equal(element, alarm);
    });
  }

  bool equal(AlarmInfo element, AlarmInfo alarm) {
    return element.time.hour == alarm.time.hour &&
        element.time.minute == alarm.time.minute &&
        element.period == alarm.period &&
        element.ringDays == alarm.ringDays;
  }

  void edit({required int alarmIndex, required BuildContext context}) {
    context.push('/edit', extra: alarmIndex);

    ringDays = appData.alarms[alarmIndex].ringDays
        .copyWith(days: appData.alarms[alarmIndex].ringDays.days);
  }

  void toggle({required int alarmIndex, required bool toggle}) {
    AlarmInfo alarm = appData.alarms[alarmIndex];

    if (toggle) {
      addToActivatedAlarms(alarm);
    } else {
      removeFromActivatedAlarms(alarm);
    }

    alarm.enabled = toggle;

    setAllAlarms();
  }

  void cancel({required BuildContext context}) {
    context.pop();
  }

  void discard({required BuildContext context, required int alarmId}) {
    Alarm.stop(alarmId);

    context.pop();
  }

  void editRingDays(
      {required EditRingDaysExtra editRingDaysExtra,
      required BuildContext context}) {
    context.push('/editRingDays', extra: editRingDaysExtra);

    checkWeekdaysAndWeekends();
  }

  void saveRingDays({required BuildContext context}) {
    context.pop();
  }

  void switchWeek({required WeekType weekType, required int toggleIndex}) {
    switch (weekType) {
      case WeekType.days:
        weekdays[toggleIndex] = !weekdays[toggleIndex];

        ringDays.switchWeekdays(weekdays[toggleIndex]);
        break;
      case WeekType.ends:
        weekends[toggleIndex] = !weekends[toggleIndex];

        ringDays.switchWeekends(weekends[toggleIndex]);
        break;
    }
  }

  void toggleRingDay({required int ringDayCardIndex}) {
    ringDays.days[ringDayCardIndex].status =
        !ringDays.days[ringDayCardIndex].status;

    checkWeekdaysAndWeekends();
  }

  void add({required BuildContext context}) {
    ringDays = RingDays();

    context.push('/add');
  }

  void delete({required int alarmIndex}) {
    AlarmInfo alarm = appData.alarms[alarmIndex];
    removeFromActivatedAlarms(alarm);

    final int incrementedAlarmIndex = alarmIndex + 1;

    cancelAlarm(alarm: alarm, alarmIndex: incrementedAlarmIndex);

    appData.alarms.removeAt(alarmIndex);

    setAllAlarms();
  }

  void cancelAlarm({
    required AlarmInfo alarm,
    required int alarmIndex,
  }) {
    if (alarm.ringDays.days.any((day) => day.status == true)) {
      alarm.ringDays.days.asMap().forEach((dayIndex, day) {
        final int incrementedDayIndex = dayIndex + 1;

        if (day.status) {
          final int alarmId = int.parse('$alarmIndex$incrementedDayIndex');

          Alarm.stop(alarmId);
        }
      });
    } else {
      final int alarmId = int.parse('${alarmIndex}0');

      Alarm.stop(alarmId);
    }
  }

  void init() {
    setAllAlarms();
  }

  void setAllAlarms() {
    sort();

    for (var alarm in appData.activatedAlarms) {
      setAlarms(alarm);
    }

    closestAlarmBloc.updateClosestAlarm();
  }

  void setAlarms(AlarmInfo alarm) async {
    if (alarm.ringDays.days.any((day) => day.status == true)) {
      alarm.ringDays.days.asMap().forEach((dayIndex, day) async {
        await setAlarm(day, dayIndex, appData.alarms.indexOf(alarm), alarm);
      });
    } else {
      await setOnceAlarm(alarm);
    }
  }

  Future<void> setOnceAlarm(AlarmInfo alarm) async {
    final int incrementedAlarmIndex = appData.alarms.indexOf(alarm) + 1;
    final int scheduleAlarmIndex = int.parse('${incrementedAlarmIndex}0');
    final int alarmHour = alarm.time.hour.remainder(12);
    final int alarmMinute = alarm.time.minute.remainder(60);

    final int convertedAlarmHour =
        getConvertedAlarmHour(alarm: alarm, alarmHour: alarmHour);

    print('converted() hour is: $convertedAlarmHour');

    DateTime now = DateTime.now();

    DateTime alarmTime = getAlarmTime(
        alarm: alarm, now: now, convertedAlarmHour: convertedAlarmHour, alarmMinute: alarmMinute);

    if (alarmTimeInPast(alarmTime, now)) {
      alarmTime = DateTime(
          now.year, now.month, now.day + 1, convertedAlarmHour, alarmMinute);
    } else {
      alarmTime = DateTime(
          now.year, now.month, now.day, convertedAlarmHour, alarmMinute);
    }

    print('setOnceAlarm() hour is: ${alarmTime.hour}');

    await set(scheduleAlarmIndex, alarmTime, alarm);
  }

  Future<void> setAlarm(
      Day day, int dayIndex, int alarmIndex, AlarmInfo alarm) async {
    if (day.status) {
      final int incrementedDayIndex = dayIndex + 1;
      final int incrementedAlarmIndex = alarmIndex + 1;
      final int scheduleAlarmIndex =
          int.parse('$incrementedAlarmIndex$incrementedDayIndex');

      print('notification id: $scheduleAlarmIndex');

      print('weekday: ${DateTime.now().weekday}');
      print(
          'dayIndex: $incrementedDayIndex and dayIndex is less then today ${incrementedDayIndex < DateTime.now().weekday}');

      // schedule alarm time calculation

      DateTime now = DateTime.now();

      final int alarmHour = alarm.time.hour.remainder(12);
      final int alarmMinute = alarm.time.minute.remainder(60);

      int convertedAlarmHour =
          getConvertedAlarmHour(alarm: alarm, alarmHour: alarmHour);

      DateTime alarmTime = getAlarmTime(
          alarm: alarm,
          now: now,
          convertedAlarmHour: convertedAlarmHour,
          alarmMinute: alarmMinute);

      const int daysInWeek = 7;
      final int currentWeekday = now.weekday;
      final int addingDayCount =
          (incrementedDayIndex - currentWeekday + daysInWeek) % 7;

      print('addingDayCount $addingDayCount');

      if (incrementedDayIndex != currentWeekday) {
        // alarmTime.add(Duration(days: addingDayCount));
        alarmTime = DateTime(now.year, now.month, now.day + addingDayCount,
            convertedAlarmHour, alarmMinute);
      } else {
        if (alarmTimeInPast(alarmTime, now)) {
          // alarmTime.add(Duration(days: addingDayCount));

          alarmTime = DateTime(
              now.year,
              now.month,
              now.day + (addingDayCount == 0 ? 7 : addingDayCount),
              convertedAlarmHour,
              alarmMinute);
        }
      }

      alarm.ringDays.days[dayIndex].time = alarmTime;

      await set(scheduleAlarmIndex, alarmTime, alarm);
    }
  }

  Future<void> set(
      int scheduleAlarmIndex, DateTime alarmTime, AlarmInfo alarm) async {
    alarm.time = alarmTime;

    print('set() hour is: ${alarmTime.hour}');

    final AlarmSettings alarmSettings = AlarmSettings(
      id: scheduleAlarmIndex,
      dateTime: alarmTime,
      assetAudioPath: 'assets/sounds/alarm_2.wav',
      loopAudio: true,
      vibrate: true,
      fadeDuration: 3.0,
      notificationTitle: 'Alarm',
      notificationBody: alarm.name.isEmpty ? 'Alarm' : alarm.name,
      enableNotificationOnKill: true,
    );

    await Alarm.set(alarmSettings: alarmSettings);
  }

  int getConvertedAlarmHour(
      {required AlarmInfo alarm, required int alarmHour}) {
    return alarm.period == TimePeriodType.am ? alarmHour : alarmHour + 12;
  }

  DateTime getAlarmTime(
      {required AlarmInfo alarm,
      required DateTime now,
      required int convertedAlarmHour,
      required int alarmMinute}) {
    return DateTime(
        now.year, now.month, now.day, convertedAlarmHour, alarmMinute);
  }

  bool alarmTimeInPast(DateTime alarmTime, DateTime now) {
    final int compareResult = alarmTime.compareTo(now); // сравниваем время

    if (compareResult <= 0) {
      return true;
    } else {
      return false;
    }
  }

  void removeFromActivatedAlarms(AlarmInfo alarm) {
    if (!alarm.enabled) return;

    final int alarmIndex = appData.activatedAlarms.indexOf(alarm);

    final activatedAlarmIncrementedIndex = alarmIndex + 1;

    cancelAlarm(alarm: alarm, alarmIndex: activatedAlarmIncrementedIndex);

    appData.activatedAlarms.removeAt(alarmIndex);
  }

  void addToActivatedAlarms(AlarmInfo alarm) {
    final int alarmIndex = appData.alarms.indexOf(alarm);

    if (appData.activatedAlarms.length < alarmIndex + 1) {
      appData.activatedAlarms.add(alarm);
    }
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'appData': appData.toMap(),
    };
  }

  factory AlarmState.fromMap(Map<String, dynamic> map) {
    return AlarmState(appData: AppData.fromMap(map['appData']));
  }

  String toJson() => json.encode(toMap());

  factory AlarmState.fromJson(String source) =>
      AlarmState.fromMap(json.decode(source) as Map<String, dynamic>);
}

final class IdleState extends AlarmState {
  IdleState({required super.appData});

  IdleState.copy(
      {required super.appData,
      required RingDays ringDays,
      List<bool>? weekdays,
      List<bool>? weekends}) {
    this.ringDays = ringDays;
    this.weekdays = weekdays ?? [false];
    this.weekends = weekends ?? [false];
  }
}

final class RingingState extends AlarmState {
  RingingState({required super.appData});
}

enum WeekType { days, ends }
