part of 'timer_cubit.dart';

class TimerState {
  late Timer timer;

  void runTimer() {
    bool switchToSecondTimer = false;

    Timer milliTimer =
        Timer.periodic(const Duration(milliseconds: 1), (milliTimer) {
      if (DateTime.now().millisecond == 0 && !switchToSecondTimer) {
        switchToSecondTimer = true;

        milliTimer.cancel();

        timer = Timer.periodic(const Duration(seconds: 1), (timer) {
          if (DateTime.now().second == 0) {
            closestAlarmBloc.updateClosestAlarm();
          }
        });
      }
    });
  }
}

final class TimerInitialState extends TimerState {}

final class TimerRunState extends TimerState {
  TimerRunState() {
    runTimer();
  }
}
