import 'dart:async';

import 'package:alarm_app/main.dart';
import 'package:bloc/bloc.dart';

part 'timer_state.dart';

class TimerCubit extends Cubit<TimerState> {
  TimerCubit() : super(TimerInitialState());

  void init() => emit(TimerRunState());
}
