// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';


import '../alarm/alarm.dart';

class AppData {
  AppData({required this.alarms, required this.activatedAlarms});

  List<AlarmInfo> alarms = [
    // AlarmInfo(
    //     name: 'Job',
    //     enabled: true,
    //     time: const Duration(hours: 1, minutes: 0),
    //     ringDays: RingDays.custom(days: [
    //       Day(name: 'Mn', status: true),
    //       Day(name: 'Tu', status: false),
    //       Day(name: 'Wd', status: false),
    //       Day(name: 'Th', status: false),
    //       Day(name: 'Fr', status: false),
    //       Day(name: 'St', status: false),
    //       Day(name: 'Sn', status: false),
    //     ]),
    //     period: TimePeriodType.am),
    // AlarmInfo(
    //     name: 'Job',
    //     enabled: true,
    //     time: const Duration(hours: 7, minutes: 0),
    //     ringDays: RingDays.weekdays(),
    //     period: TimePeriodType.pm),
    // AlarmInfo(
    //     name: 'Courses',
    //     enabled: true,
    //     time: const Duration(hours: 4, minutes: 0),
    //     ringDays: RingDays.everyDay(),
    //     period: TimePeriodType.pm),
    // AlarmInfo(
    //     name: 'Gym',
    //     enabled: true,
    //     time: const Duration(hours: 6, minutes: 2),
    //     ringDays: RingDays(),
    //     period: TimePeriodType.am),
    // AlarmInfo(
    //     name: 'Alarm Name',
    //     enabled: false,
    //     time: const Duration(hours: 4, minutes: 4),
    //     ringDays: RingDays.weekends(),
    //     period: TimePeriodType.pm),
    // AlarmInfo(
    //     name: 'Alarm Name',
    //     enabled: false,
    //     time: const Duration(hours: 5, minutes: 0),
    //     ringDays: RingDays(),
    //     period: TimePeriodType.am),
    // AlarmInfo(
    //     name: 'Alarm Name',
    //     enabled: false,
    //     time: const Duration(hours: 6, minutes: 0),
    //     ringDays: RingDays.everyDay(),
    //     period: TimePeriodType.am),
    // AlarmInfo(
    //     name: 'Alarm Name',
    //     enabled: false,
    //     time: const Duration(hours: 7, minutes: 0),
    //     ringDays: RingDays.everyDay(),
    //     period: TimePeriodType.am),
    // AlarmInfo(
    //     name: 'Alarm Name',
    //     enabled: false,
    //     time: const Duration(hours: 8, minutes: 2),
    //     ringDays: RingDays(),
    //     period: TimePeriodType.pm),
    // AlarmInfo(
    //     name: 'Alarm Name',
    //     enabled: false,
    //     time: const Duration(hours: 9, minutes: 4),
    //     ringDays: RingDays(),
    //     period: TimePeriodType.am),
    // AlarmInfo(
    //     name: 'Alarm Name',
    //     enabled: false,
    //     time: const Duration(hours: 10, minutes: 0),
    //     ringDays: RingDays(),
    //     period: TimePeriodType.pm),
  ];

  List<AlarmInfo> activatedAlarms = [
    // AlarmInfo(
    //     name: 'Job',
    //     enabled: true,
    //     time: const Duration(hours: 1, minutes: 0),
    //     ringDays: RingDays.custom(days: [
    //       Day(name: 'Mn', status: true),
    //       Day(name: 'Tu', status: false),
    //       Day(name: 'Wd', status: false),
    //       Day(name: 'Th', status: false),
    //       Day(name: 'Fr', status: false),
    //       Day(name: 'St', status: false),
    //       Day(name: 'Sn', status: false),
    //     ]),
    //     period: TimePeriodType.am),
    // AlarmInfo(
    //     name: 'Job',
    //     enabled: true,
    //     time: const Duration(hours: 7, minutes: 0),
    //     ringDays: RingDays.weekdays(),
    //     period: TimePeriodType.pm),
    // AlarmInfo(
    //     name: 'Courses',
    //     enabled: true,
    //     time: const Duration(hours: 4, minutes: 0),
    //     ringDays: RingDays.everyDay(),
    //     period: TimePeriodType.pm),
  ];

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'alarms': alarms.map((x) => x.toMap()).toList(),
      'activatedAlarms': activatedAlarms.map((x) => x.toMap()).toList(),
    };
  }

  factory AppData.fromMap(Map<String, dynamic> map) {
    return AppData(
      alarms: List<AlarmInfo>.from((map['alarms']).map<AlarmInfo>((x) => AlarmInfo.fromMap(x as Map<String,dynamic>),),),
      activatedAlarms: List<AlarmInfo>.from((map['activatedAlarms']).map<AlarmInfo>((x) => AlarmInfo.fromMap(x as Map<String,dynamic>),),),
    );
  }

  String toJson() => json.encode(toMap());

  factory AppData.fromJson(String source) => AppData.fromMap(json.decode(source) as Map<String, dynamic>);
}
