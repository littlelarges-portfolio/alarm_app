import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'ring_days.dart';

class AlarmInfo extends Equatable {
  AlarmInfo(
      {required this.name,
      required this.enabled,
      required this.time,
      required this.period,
      required this.ringDays});

  final String name;
  DateTime time;
  final TimePeriodType period;
  final RingDays ringDays;
  bool enabled;
  
  @override
  List<Object?> get props => [name, time, period, ringDays];

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'name': name,
      'enabled': enabled,
      'time': time.toString(),
      'period': period.value.index,
      'ringDays': ringDays.toMap(),
    };
  }

  factory AlarmInfo.fromMap(Map<String, dynamic> map) {
    return AlarmInfo(
      name: map['name'] as String,
      period: TimePeriodType.values[map['period'] as int],
      ringDays: RingDays.fromMap(map['ringDays'] as Map<String,dynamic>),
      time: DateTime.parse(map['time']), enabled: map['enabled'], 
    );
  }

  String toJson() => json.encode(toMap());

  factory AlarmInfo.fromJson(String source) => AlarmInfo.fromMap(json.decode(source) as Map<String, dynamic>);
}

enum TimePeriodType {
  am(TimePeriod(name: 'am', index: 0)),
  pm(TimePeriod(name: 'pm', index: 1));

  const TimePeriodType(this.value);
  final TimePeriod value;
}

class TimePeriod {
  const TimePeriod({required this.name, required this.index});

  final String name;
  final int index;
}
