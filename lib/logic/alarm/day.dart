// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:equatable/equatable.dart';

class Day extends Equatable {
  Day({required this.name, this.status = false});

  final String name;
  bool status;
  late DateTime time;
  
  @override
  List<Object?> get props => [name, status];

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'name': name,
      'status': status,
    };
  }

  factory Day.fromMap(Map<String, dynamic> map) {
    return Day(
      name: map['name'] as String,
      status: map['status'] as bool,
    );
  }

  String toJson() => json.encode(toMap());

  factory Day.fromJson(String source) => Day.fromMap(json.decode(source) as Map<String, dynamic>);
}
