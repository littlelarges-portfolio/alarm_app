import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'day.dart';

class RingDays extends Equatable {
  RingDays();

  RingDays.from({required List<Day> days});

  RingDays.custom({required this.days});

  List<Day> days = [
    Day(name: 'Mn'),
    Day(name: 'Tu'),
    Day(name: 'Wd'),
    Day(name: 'Th'),
    Day(name: 'Fr'),
    Day(name: 'St'),
    Day(name: 'Sn'),
  ];

  RingDays.weekdays() {
    days.asMap().forEach((index, day) {
      if (index < 5) {
        day.status = true;
      } else {
        day.status = false;
      }
    });
  }

  RingDays.weekends() {
    days.asMap().forEach((index, day) {
      if (index < 5) {
        day.status = false;
      } else {
        day.status = true;
      }
    });
  }

  RingDays.everyDay() {
    days.asMap().forEach((index, day) {
      day.status = true;
    });
  }

  bool allRingDaysFalse() {
    for (Day day in days) {
      if (day.status != false) {
        return false;
      }
    }

    return true;
  }

  bool weekdayRingDaysTrue() {
    for (int i = 0; i < 5; i++) {
      if (days[i].status != true) {
        return false;
      }
    }
    return true;
  }

  bool weekendRingDaysTrue() {
    for (int i = 5; i < 7; i++) {
      if (days[i].status != true) {
        return false;
      }
    }
    return true;
  }

  void switchWeekdays(bool value) {
    days.asMap().forEach((index, day) {
      if (index < 5) {
        day.status = value;
      }
    });
  }

  void switchWeekends(bool value) {
    days.asMap().forEach((index, day) {
      if (index >= 5) {
        day.status = value;
      }
    });
  }

  // deep copy
  RingDays copyWith({List<Day>? days}) {
    return RingDays.custom(
      days: days != null
          ? List<Day>.from(
              days.map((day) => Day(name: day.name, status: day.status)))
          : List<Day>.from(
              this.days.map((day) => Day(name: day.name, status: day.status))),
    );
  }

  @override
  List<Object?> get props => [days];

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'days': days.map((x) => x.toMap()).toList(),
    };
  }

  factory RingDays.fromMap(Map<String, dynamic> map) {
    return RingDays.from(
      days: List<Day>.from((map['days']).map<Day>((x) => Day.fromMap(x as Map<String,dynamic>),),),
    );
  }

  String toJson() => json.encode(toMap());

  factory RingDays.fromJson(String source) =>
      RingDays.fromMap(json.decode(source) as Map<String, dynamic>);
}
