import 'package:alarm/model/alarm_settings.dart';
import 'package:alarm_app/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import '../../presentation/components/edit_page/select_ring_days_dialog.dart';
import '../../presentation/pages/discard_page.dart';
import '../../presentation/pages/edit_page.dart';
import '../../presentation/pages/home_page.dart';

class AppRouter {
  GoRouter router = GoRouter(routes: [
    GoRoute(
      path: '/',
      builder: (context, state) => MultiBlocProvider(providers: [
        BlocProvider.value(value: alarmBloc),
        BlocProvider.value(value: timerBloc),
        BlocProvider.value(value: closestAlarmBloc),
      ], child: const HomePage()),
    ),
    GoRoute(
      path: '/discard',
      builder: (context, state) =>
          DiscardPage(alarmSettings: state.extra as AlarmSettings),
    ),
    GoRoute(
      path: '/edit',
      builder: (context, state) => EditPage(alarmIndex: state.extra as int),
    ),
    GoRoute(
      path: '/editRingDays',
      pageBuilder: (context, state) {
        final EditRingDaysExtra extra = state.extra as EditRingDaysExtra;

        return CustomTransitionPage(
            opaque: false,
            barrierColor: Colors.black.withOpacity(.4),
            child: BlocProvider.value(
              value: alarmBloc,
              child: SelectRingDaysDialog(
                context: context,
                alarmIndex: extra.alarmIndex,
                alarmInfoStyle: extra.alarmInfoStyle,
                ringDays: extra.ringDays,
              ),
            ),
            transitionDuration: const Duration(milliseconds: 100),
            reverseTransitionDuration: const Duration(milliseconds: 100),
            transitionsBuilder: (BuildContext context,
                Animation<double> animation,
                Animation<double> secondaryAnimation,
                Widget child) {
              return child;
            });
      },
    ),
    GoRoute(path: '/add', builder: (context, state) => EditPage.add()),
  ]);
}
