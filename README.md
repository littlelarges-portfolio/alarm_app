# Alarm App

Alarm app with several features

<img src="https://i.imgur.com/CGJblQM.png" width="188" height="354">

To add a new alarm:

1. Click the “+” button on the main page.
2. Select the desired time by spinning the wheels
3. If you wish, you can give a name, only 10 characters are available
4. Select call days or leave blank for a single call

Developed using the Bloc package, also used HydratedBloc

<img src="https://i.imgur.com/HjpRkJF.png" width="188" height="354"><img src="https://i.imgur.com/1pPYz6P.png" width="188" height="354"><img src="https://i.imgur.com/0ndIrLi.png" width="188" height="354"><img src="https://i.imgur.com/kMfOpA4.png" width="188" height="354"><img src="https://i.imgur.com/CGJblQM.png" width="188" height="354"><img src="https://i.imgur.com/GLv9AhT.png" width="188" height="354"><img src="https://i.imgur.com/UnP8MvY.png" width="188" height="354"><img src="https://i.imgur.com/drtbx8W.png" width="188" height="354"><img src="https://i.imgur.com/amyzrjY.png" width="188" height="354">

The application has a function for calculating the nearest alarm clock.

2 alarms are also available, single and repeating on selected days of the week. Single alarm turns off after first dismissal

When selecting ring days, the weekdays and weekends quick selection buttons are available to select only weekdays or only weekends, or all together

There is also a convenient sorting of alarm clocks. Alarm clocks that are closer to 12 am are lined up on top. Disabled alarms are sorted according to the same rule, only below all enabled ones.

It is possible to remove alarms and add them only if there is no duplicate alarm.

Demo:

<img src="demo/alarm_app_demo.gif" width="188" height="354">